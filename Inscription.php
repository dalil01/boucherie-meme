<?php
     include('Connect/connect.php');
	 
	   if(isset($_POST['buttoninscription']))				 
{
   $nom = htmlspecialchars (trim(ucfirst($_POST['nom'])));
   $prenom = htmlspecialchars (trim(ucfirst($_POST['prenom'])));
   $mail = htmlspecialchars(trim($_POST['mail']));
   $mdp = sha1($_POST['mdp']);
   $mdp2 = sha1($_POST['mdp2']);    
   
  if(!empty($_POST['nom'])) {
   if(!empty($_POST['prenom'])) {   
	if(!empty($_POST['mail'])) {
	 if(filter_var($mail, FILTER_VALIDATE_EMAIL)) {
		 
	      $reqmail = $bdd->prepare("SELECT * FROM clients WHERE mail = ?");
          $reqmail->execute(array($mail));
          $mailexist = $reqmail->rowCount();
       if($mailexist == 0) {	 
     if(!empty($_POST['mdp'])) {		
	  if(!empty($_POST['mdp2'])) {  
	   if( $mdp == $mdp2){
		   
		    $insertclts = $bdd->prepare("INSERT INTO clients(prenom, nom, mail, mdp) VALUES(?, ?, ?, ?)");
            $insertclts->execute(array($prenom, $nom, $mail, $mdp));
   
                     $msg = "Votre compte a bien été créé !";
	 
	  } else {
         $msg = "Vos mots de passes ne correspondent pas. Veuillez réessayer !";
      }
	 } else {
     $msgmdp2 = "Veuillez confirmer votre mot de passe"; 
  } 
} else {
     $msgmdp = "mot de passe"; 
  }    	  
 } else {
       $msgmail = "Adresse mail déjà utilisée !";
      }
   } else {
       $msgmail = "Il semble que vous ayez saisi une adresse e-mail incorrecte. Veuillez la corriger et cliquez pour continuer !";
     }
	} else {
     $msgmail = "mail"; 
  }    
  } else {
     $msgprenom = "prenom"; 
  }
   } else {
     $msgnom = "nom"; 
  } 
}
?>
<! DOCTYPE html>
<html>

<head>
<meta charset='UTF-8'/>
<title>Boucherie MEME</title>
<link href="Css/Css.css" type="text/css" rel="stylesheet"/>
</head>

<body>

<div id="inscription">
  <h2>Inscription</h2>
<form method="post">
<table> 
<tr>
<td><input type="text" placeholder="Nom" name="nom" value="<?php if(isset($nom)) { echo $nom; } ?>" />
<td>
<?php
if(isset($msgnom))
   { 
	   echo "<div class='msgnom'>".$msgnom."</div>";
   }
?>
</td>
</td>
</tr>
<tr>
<td><input type="text" placeholder="Prénom" name="prenom" value="<?php if(isset($prenom)) { echo $prenom; } ?>" /></td>
<td>
<?php
if(isset($msgprenom))
   { 
	   echo "<div class='msgprenom'>".$msgprenom."</div>";
   }
?>
</td>
</tr>
<tr>
<td><input id="mail" type="email" placeholder="E-mail" id="mail" name="mail" value="<?php if(isset($mail)) { echo $mail; } ?>" /></td>
<td>
<?php
if(isset($msgmail))
   { 
	   echo "<div class='msgmail'>".$msgmail."</div>";
   }
?>
</td>
</tr>
<tr>
<td><input id="mdp" type="password" placeholder="Mot de passe" name="mdp" /></td>
<td>
<?php
if(isset($msgmdp))
   { 
	   echo "<div class='msgmdp'>".$msgmdp."</div>";
   }
?>
</td>
</tr>
<tr>
<td><input id="mdp2" type="password" placeholder="Confirmer votre mot de passe" name="mdp2" /></td>
<td>
<?php
if(isset($msgmdp2)) 
   { 
	   echo "<div class='msgmdp2'>".$msgmdp2."</div>";
   }
?>
</td>
</tr>
<tr>
<td><input id="submit" type="submit" name="buttoninscription" value="Je m'inscris" /></td>
</tr>
</table>
<?php
if(isset($msg))
   { 
	   echo "<div class='msg'>".$msg."</div>";
   }
?>
</form>
</div>

</body>

</html>
