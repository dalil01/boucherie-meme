<?php
session_start();
        include('Connect/connect.php');
		
	if(isset($_POST['buttonconnexion']))
{
    $mailconnect = htmlspecialchars($_POST['mailconnect']);
    $mdpconnect = sha1($_POST['mdpconnect']);
	
	 if(!empty($mailconnect) AND !empty($mdpconnect)) {
		 
	  $requser = $bdd->prepare("SELECT * FROM clients WHERE mail = ? AND mdp = ?");
      $requser->execute(array($mailconnect, $mdpconnect));
      $userexist = $requser->rowCount();
      if($userexist == 1) {
		  
		    $userinfo = $requser->fetch();
            $_SESSION['id'] = $userinfo['id'];
            $_SESSION['prenom'] = $userinfo['prenom'];
            $_SESSION['nom'] = $userinfo['nom'];
            $_SESSION['mail'] = $userinfo['mail'];
			$_SESSION['mdp'] = $userinfo['mdp'];
						
           header("Location: Profil_meme.php");
				exit;
		 
	} else {
         $msgconnect = "L’e-mail ou le mot de passe entré ne correspondent à aucun compte !";
	} 	 
	} else {
      $msgconnect = "Tous les champs doivent être complétés !";
   }	 
}		  
?>
<! DOCTYPE html>
<html>

<head>
<meta charset='UTF-8'/>
<title>Boucherie MEME</title>
<link href="css.css" type="text/css" rel="stylesheet"/>
</head>

<body>

<div id="inscription">
 <h2>Connexion</h2>
<form method="post">
<table> 
<tr>
<td><input id="mail" type="email"  placeholder="E-mail" id="mail" name="mailconnect" value="<?php if(isset($mailconnect)) { echo $mailconnect; } ?>" /></td>
</tr>
<tr>
<td><input id="mdp" type="password" placeholder="Mot de passe" name="mdpconnect" /></td>
</tr>
<tr>
<td><input id="submit" type="submit" name="buttonconnexion" value="Je m'inscris" /></td>
</tr>
<tr>
<td><a href="" id="oublie">Vous avez oublié votre mot de passe ?</a></td>
</tr>
</table>
</form>
<?php
  if(isset($msgconnect))
  {
	  echo '<div class="msgconnect">'.$msgconnect."</div>";
  }
?> 
</div>

</body>

</html>
