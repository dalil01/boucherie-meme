<?php
session_start();
        include('Connect/connect.php');
		
	if(isset($_POST['buttonconnexion']))
{
    $mailconnect = htmlspecialchars($_POST['mailconnect']);
    $mdpconnect = sha1($_POST['mdpconnect']);
	
	 if(!empty($mailconnect) AND !empty($mdpconnect)) {
		 
	  $requser = $bdd->prepare("SELECT * FROM clients WHERE mail = ? AND mdp = ?");
      $requser->execute(array($mailconnect, $mdpconnect));
      $userexist = $requser->rowCount();
      if($userexist == 1) {
		  
		    $userinfo = $requser->fetch();
            $_SESSION['id'] = $userinfo['id'];
            $_SESSION['prenom'] = $userinfo['prenom'];
            $_SESSION['nom'] = $userinfo['nom'];
            $_SESSION['mail'] = $userinfo['mail'];
			$_SESSION['mdp'] = $userinfo['mdp'];
						
           header("Location: Profil_meme.php");
				exit;
		 
	} else {
         $msgconnect = "L’e-mail ou le mot de passe entré ne correspondent à aucun compte !";
	} 	 
	} else {
      $msgconnect = "Tous les champs doivent être complétés !";
   }	 
}		  
?>
<! DOCTYPE html>
<html>

<head>
<meta charset='UTF-8'/>
<title> Boucherie MEME </title>
<link href="Css/css.css" type="text/css" rel="stylesheet"/>
</head>

<body>

<div id="header">
<div id="contenu">

<table>
<tr></tr><tr></tr>
<tr>
<td>
<img src="Image/Logo.png" width="70" height="70" >
</td>
<td>
<h1>Boucherie MEME</h1>
</td>
<tr>
</table>

<div id="connexion">
<form method="post">
<table> 
<tr>
<td><input id="mail" type="email" size="25" placeholder="E-mail" id="mail" name="mailconnect" value="<?php if(isset($mailconnect)) { echo $mailconnect; } ?>" />
<input id="mdp" type="password" placeholder="Mot de passe" name="mdpconnect" /></td>
<td><input id="submit" type="submit" name="buttonconnexion" value="Connexion" /></td>
</tr>
<tr>
<td align="center"><a href="" id="oublie">Vous avez oublié votre mot de passe ?</a></td>
</tr>
</table>
</form>
<?php
  if(isset($msgconnect))
  {
	  echo '<div class="msgconnect">'.$msgconnect."</div>";
  }
?> 
</div>
</div>
</div>

</div>

<div id="body">

<div class="Info"> 
<h3>Adresse :</h3>
<p>8 Rue Grande, 77250 Moret sur Loing</p>
<h3>Horaire :</h3>
<p>Lundi Fermé</p> 
<p>Mardi 08:30–12:30 <br> 16:00–19:30 </p>
<p>Mercredi 08:30–12:30 </p>
<p>Jeudi 08:30–12:30 <br> 16:00–19:30</p>
<p>Vendredi : 08:30–12:30 <br> 16:00–19:30</p>
<p>Samedi : 08:00–12:30 <br> 15:30–19:30</p>
<p>Dimanche : 08:30–12:30</p>
</p>
<h3>Téléphone :</h3>
<p> 01 60 70 50 51</p>
</div>

<!-- Container for the image gallery -->
<div class="container">

  <!-- Full-width images with number text -->
  <div class="mySlides">
      <img src="Image/diapo__1.jpg" style="width:600px; height:350px;">
  </div>

  <div class="mySlides">
      <img src="Image/diapo__2.jpg" style="width:600px; height:350px;">
  </div>

  <div class="mySlides">
      <img src="Image/diapo__3.jpg" style="width:600px; height:350px;">
  </div>
  
  <div class="mySlides">
      <img src="Image/diapo__4.jpg" style="width:600px; height:350px;">
  </div>

  <!-- Image text -->
  <div class="caption-container">
    <p id="caption"></p>
  </div>

  <!-- Thumbnail images -->
  <div class="row">
    <div class="column">
      <img class="demo cursor" src="Image/diapo__1.jpg" style="width:125px; height: 100px;" onclick="currentSlide(1)" alt="The Woods">
    </div>
    <div class="column"> 
      <img class="demo cursor" src="Image/diapo__2.jpg" style="width:125px; height: 100px;" onclick="currentSlide(2)" alt="Cinque Terre">
    </div>
    <div class="column">
      <img class="demo cursor" src="Image/diapo__3.jpg" style="width:125px; height: 100px;" onclick="currentSlide(3)" alt="Mountains and fjords">
    </div>
	<div class="column">
      <img class="demo cursor" src="Image/diapo__4.jpg" style="width:125px; height: 100px;" onclick="currentSlide(4)" alt="Mountains and fjords">
    </div>
  </div>
</div>
<script>
var slideIndex = 1;
showSlides(slideIndex);

// Next/previous controls
function plusSlides(n) {
  showSlides(slideIndex += n);
}

// Thumbnail image controls
function currentSlide(n) {
  showSlides(slideIndex = n);
}

function showSlides(n) {
  var i;
  var slides = document.getElementsByClassName("mySlides");
  var dots = document.getElementsByClassName("demo");
  var captionText = document.getElementById("caption");
  if (n > slides.length) {slideIndex = 1}
  if (n < 1) {slideIndex = slides.length}
  for (i = 0; i < slides.length; i++) {
    slides[i].style.display = "none";
  }
  for (i = 0; i < dots.length; i++) {
    dots[i].className = dots[i].className.replace(" active", "");
  }
  slides[slideIndex-1].style.display = "block";
  dots[slideIndex-1].className += " active";
  captionText.innerHTML = dots[slideIndex-1].alt;
}
</script>

<div class="form">
<button><a href="Inscription.html">Inscription</a></button>
</div>

<div class="topnav">
  <a class="active" href="Nos_produits.html">Nos produits-Réservation</a>
  <a href="Recettes.html">Recettes</a>
</div>

<div id="footer">
<div id="ndt">
<a href="Notre_equipe"><p>Notre équipe</p></a>
<a href="Nos_fournisseurs.html"><p>Nos fournisseurs</p></a>
</div>
<div id= "slogan"> <h3 class="foottitle">POUR VOTRE SANTÉ,</h3> 
<h4 class="slo1">ÉVITEZ DE MANGER : </h4>
 <h4 class="slo2"> TROP GRAS &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; TROP SUCRÉ &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; TROP SALÉ</h4>
<h4 class="slo3">L’ABUS D’ALCOOL EST DANGEREUX POUR LA SANTÉ, <br> CONSOMMEZ AVEC MODÉRATION.</h4>
</div>
<p class= "reseau">SUIVEZ-NOUS SUR NOS RÉSEAUX SOCIAUX</p>
<a href="https://fr-fr.facebook.com/pages/category/Butcher-Shop/Boucherie-M%C3%AAme-77250-193310294651518/"><img class="imgfb" src="Image/fb.jpg"></a>
<a href="https://www.instagram.com/boucherie_meme/"><img class="imginsta" src="Image/insta.jpg"></a>
</div>

</body>

</html>
